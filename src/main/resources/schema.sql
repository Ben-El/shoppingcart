drop table if exists Beef;
create table Beef(id int,
                  name varchar2(30),
                  price float,
                  weight float);

drop table if exists Chicken;
create table Chicken(id int,
                     name varchar2(30),
                     price float,
                     weight float);

drop table if exists Cheese;
create table Cheese(id int,
                    name varchar2(30),
                    price float,
                    quantity int);

drop table if exists Milk;
create table Milk(id int,
                  name varchar2(30),
                  price float,
                  quantity int);