insert into Beef (id, name, price, weight) values
(0, 'Asado', 93.07, 1.434),
(1, 'Lamb_Ribs', 100, 1),
(2, 'Entrecote', 70, 0.5),
(3, 'Sinta_Slim_Slices', 62.06, 0.414);

insert into Chicken (id, name, price, weight) values
(4, 'Turkey', 20.7, 0.301),
(5, 'Wings', 150, 4),
(6, 'Pullet', 86.7, 2),
(7, 'Brisket', 300, 11.688);

insert into Cheese (id, name, price, quantity) values
(8, 'Mozzarella', 10, 1),
(9, 'Ricotta', 15, 20),
(10, 'Brie', 20, 168),
(11, 'Feta', 25, 60);

insert into Milk (id, name, price, quantity) values
(12, 'Tara', 5, 1000),
(13, 'Tnuva', 6, 500),
(14, 'Yotvata', 7, 70),
(15, 'Coconut_Milk', 8, 2),
(16, 'Almond_Milk', 10, 100),