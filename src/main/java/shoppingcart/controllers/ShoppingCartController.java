package shoppingcart.controllers;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shoppingcart.Cart;
import shoppingcart.products.Product;
import shoppingcart.services.*;

import java.util.List;

@RestController
public class ShoppingCartController {

    @Autowired
    private Cart shopCart;

    @Autowired
    private DSLContext context;

    @Autowired
    private DbService dbService;

    @Autowired
    private BillCalculator billCalculator;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private OrderDetailsService orderDetailsService;

    @Autowired
    private ProductsService productsService;

    @Autowired
    public void initializeProductList() {
        productsService.loadProductsFromDb(context, dbService);
    }

    @GetMapping("/allProducts")
    public ResponseEntity<List<Product>> getAllProducts() {
        return productsService.getAllProducts();
    }

    @PostMapping("/addProductToCart")
    public String getSpecificProductById(@RequestParam(value = "productId") int productId) {
        return productsService.addProductToCart(shopCart, productId);
    }

//    @PostMapping("/addProductToCart")
//    public String getSpecificProductById(@RequestBody Map<String, Integer> productIdMap) {
//        return productsService.addProductToCart(shopCart, productIdMap.get("id"));
//    }

//    @PostMapping("/addProductToCart")
//    public String getSpecificProductById(@RequestBody int productIdMap) {
//        return productsService.addProductToCart(shopCart, productIdMap);
//    }

    @GetMapping("/showOrderDetails")
    public ResponseEntity<Object> showOrderDetails() {
        return orderDetailsService.showOrderDetails(shopCart, billCalculator);
    }

    @PostMapping("/pay")
    public ResponseEntity<Object> pay(@RequestParam(value = "moneySumPaid") double moneySumPaid) {
        ResponseEntity<Object> objectResponseEntity = paymentService.pay(shopCart, billCalculator, moneySumPaid);

        if (objectResponseEntity.getStatusCodeValue() == HttpStatus.OK.value()) {
            dbService.updateProductsQuantities(context, shopCart);
            shopCart.getShopCart().clear();
        }

        return objectResponseEntity;
    }

    @PostMapping("/resetCart")
    public String resetCart() {
        shopCart.getShopCart().clear();
        return "Your cart has been cleared.";
    }
}