package shoppingcart.exceptions;

public class OutOfMoneyException extends Exception {

    public OutOfMoneyException() {
        super("Not enough money was paid.");
    }
}