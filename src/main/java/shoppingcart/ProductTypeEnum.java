package shoppingcart;

public enum ProductTypeEnum {
    BEEF,
    MILK,
    CHEESE,
    CHICKEN
}