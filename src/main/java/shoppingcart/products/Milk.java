package shoppingcart.products;

import shoppingcart.ProductTypeEnum;

public class Milk extends DairyProduct {

    public Milk(int id, String name, double price, int quantity) {
        super(id, name, price, quantity);
        setType(ProductTypeEnum.MILK.name());
    }

    @Override
    public String toString() {
        return String.format("Id: " + getId() + ", Name: " + getName() + ", Price: " + getPrice());
    }
}