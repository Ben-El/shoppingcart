package shoppingcart.products;

import shoppingcart.ProductTypeEnum;

public class Cheese extends DairyProduct {

    public Cheese(int id, String name, double price, int quantity) {
        super(id, name, price, quantity);
        setType(ProductTypeEnum.CHEESE.name());
    }

    @Override
    public String toString() {
        return String.format("Id: " + getId() + ", Name: " + getName() + ", Price: " + getPrice());
    }
}