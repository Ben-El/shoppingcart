package shoppingcart.products;

import lombok.Getter;
import lombok.Setter;

public class DairyProduct extends Product {

    @Getter
    @Setter
    private int quantity;

    public DairyProduct(int id, String name, double price, int quantity) {
        super(id, name, price);
        setQuantity(quantity);
    }

    @Override
    public String toString() {
        return String.format("Id: " + getId() + ", Name: " + getName() + ", Price: " + getPrice());
    }
}