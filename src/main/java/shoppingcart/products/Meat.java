package shoppingcart.products;

import lombok.Getter;
import lombok.Setter;

public class Meat extends Product {

    @Getter
    @Setter
    private double weight;

    public Meat(int id, String name, double price, double weight) {
        super(id, name, price);
        setWeight(weight);
    }

    @Override
    public String toString() {
        return String.format("Id: " + getId() + ", Name: " + getName() + ", Price: " + getPrice());
    }
}