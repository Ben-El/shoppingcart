package shoppingcart.products;

import lombok.Getter;
import lombok.Setter;

@Getter
public abstract class Product {
    private final int id;
    private final String name;
    private final double price;

    @Setter
    private String type;

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public abstract String toString();
}