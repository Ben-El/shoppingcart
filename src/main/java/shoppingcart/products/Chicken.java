package shoppingcart.products;

import shoppingcart.ProductTypeEnum;

public class Chicken extends Meat {

    public Chicken(int id, String name, double price, double weight) {
        super(id, name, price, weight);
        setType(ProductTypeEnum.CHICKEN.name());
    }

    @Override
    public String toString() {
        return String.format("Id: " + getId() + ", Name: " + getName() + ", Price: " + getPrice() + ", Weight: " + getWeight());
    }
}