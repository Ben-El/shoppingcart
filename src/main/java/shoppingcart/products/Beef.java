package shoppingcart.products;

import shoppingcart.ProductTypeEnum;

public class Beef extends Meat {

    public Beef(int id, String name, double price, double weight) {
        super(id, name, price, weight);
        setType(ProductTypeEnum.BEEF.name());
    }

    @Override
    public String toString() {
        return String.format("Id: " + getId() + ", Name: " + getName() + ", Price: " + getPrice() + ", Weight: " + getWeight());
    }
}