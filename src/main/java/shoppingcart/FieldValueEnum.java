package shoppingcart;

public enum FieldValueEnum {
    ID,
    NAME,
    PRICE,
    WEIGHT,
    QUANTITY
}