package shoppingcart;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import shoppingcart.products.Product;

import java.util.ArrayList;
import java.util.List;

@Component
@Getter
@Setter
public class Cart {

    private List<Product> shopCart = new ArrayList<>();

    public void addSingleProduct(Product product) {
        shopCart.add(product);
    }

    @Override
    public String toString() {
        String cartProductsDetails = "";

        for (Product product : shopCart) {
            cartProductsDetails += product.toString() + "\n";
        }

        return cartProductsDetails;
    }
}