package shoppingcart.services;

import org.springframework.stereotype.Service;
import shoppingcart.Cart;
import shoppingcart.products.Product;

@Service
public class BillCalculator {
    public double calculateShopCartBill(Cart shopCart) {
        float totalBill = 0;

        for (Product product : shopCart.getShopCart()) {
            totalBill += product.getPrice();
        }

        return totalBill;
    }
}