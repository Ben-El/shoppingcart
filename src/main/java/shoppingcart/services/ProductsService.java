package shoppingcart.services;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import shoppingcart.Cart;
import shoppingcart.ProductTypeEnum;
import shoppingcart.products.DairyProduct;
import shoppingcart.products.Product;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.jooq.impl.DSL.table;

@Service
public class ProductsService {

    private final List<Product> allProducts;

    public ProductsService() {
        this.allProducts = new ArrayList<>();
    }

    public void loadProductsFromDb(DSLContext context, DbService dbRetriever) {
        Result<Record> milkTable = context.select().from(table(ProductTypeEnum.MILK.name().toLowerCase())).fetch();
        Result<Record> beefTable = context.select().from(table(ProductTypeEnum.BEEF.name().toLowerCase())).fetch();
        Result<Record> cheeseTable = context.select().from(table(ProductTypeEnum.CHEESE.name().toLowerCase())).fetch();
        Result<Record> chickenTable = context.select().from(table(ProductTypeEnum.CHICKEN.name().toLowerCase())).fetch();

        this.allProducts.addAll(dbRetriever.retrieveTableRows(milkTable, ProductTypeEnum.MILK));
        this.allProducts.addAll(dbRetriever.retrieveTableRows(beefTable, ProductTypeEnum.BEEF));
        this.allProducts.addAll(dbRetriever.retrieveTableRows(cheeseTable, ProductTypeEnum.CHEESE));
        this.allProducts.addAll(dbRetriever.retrieveTableRows(chickenTable, ProductTypeEnum.CHICKEN));

        this.allProducts.sort(Comparator.comparing(Product::getId));
    }

    public ResponseEntity<List<Product>> getAllProducts() {
        allProducts.removeIf(product ->
                product instanceof DairyProduct &&
                        ((DairyProduct) product).getQuantity() <= 0);

        return ResponseEntity.ok().body(allProducts);
    }

    public String addProductToCart(Cart shopCart, int productId) {
        try {
            Product product = allProducts.stream().filter(p -> p.getId() == productId).findAny().get();

            if (product instanceof DairyProduct) {
                if (((DairyProduct) product).getQuantity() > 0) {
                    ((DairyProduct) product).setQuantity(((DairyProduct) product).getQuantity() - 1);
                    allProducts.set(allProducts.indexOf(product), product);
                } else {
                    return "You can't purchase more '" + product.getName() +
                            "' (id: " + productId + "). You've purchased the maximum quantity.";
                }
            }

            shopCart.addSingleProduct(product);

            return product.getName() + " added to cart.";
        } catch (Exception e) {
            return "Error in getting product that his id is: " + productId +
                    " --> The product doesn't exist.";
        }
    }
}