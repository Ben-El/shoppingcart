package shoppingcart.services;

import org.springframework.stereotype.Service;
import shoppingcart.exceptions.OutOfMoneyException;
import org.springframework.http.ResponseEntity;
import shoppingcart.Receipt;
import shoppingcart.Cart;

@Service
public class PaymentService {
    public ResponseEntity<Object> pay(Cart shopCart, BillCalculator billCalculator, double moneySumPaid) {
        double totalBill = billCalculator.calculateShopCartBill(shopCart);
        Receipt receipt = new Receipt(shopCart, totalBill);

        if (moneySumPaid >= totalBill) {
            return ResponseEntity.ok().body("Your cart: \n" + receipt.getShopCart() +
                    "\nTotal bill: " + receipt.getTotalBillSum() +
                    "\nYour payment: " + moneySumPaid +
                    "\nChange: " + (moneySumPaid - totalBill));
        }

        if (moneySumPaid <= 0) {
            return ResponseEntity.badRequest().body("Please enter a valid amount of money.");
        }

        return ResponseEntity.badRequest().body(new OutOfMoneyException().getMessage() +
                    "\nTotal bill: " + receipt.getTotalBillSum() +
                    "\nYour payment: " + moneySumPaid +
                    "\nYou need more " + (totalBill - moneySumPaid) + " to cover the payment.");
    }
}