package shoppingcart.services;

import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.stereotype.Service;
import shoppingcart.Cart;
import shoppingcart.FieldValueEnum;
import shoppingcart.ProductTypeEnum;
import shoppingcart.products.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

@Service
@Slf4j
public class DbService {
    public Collection<? extends Product> retrieveTableRows(Result<Record> rs, ProductTypeEnum className) {
        List<Product> productList = new ArrayList<>();

        for (int i = 0; i < rs.size(); i++) {
            int id = (int) rs.get(i).getValue(FieldValueEnum.ID.name());
            String name = (String) rs.get(i).getValue(FieldValueEnum.NAME.name());
            double price = (double) rs.get(i).getValue(FieldValueEnum.PRICE.name());

            try {
                double weight = (double) rs.get(i).getValue(FieldValueEnum.WEIGHT.name());
                switch (className) {
                    case BEEF:
                        productList.add(new Beef(id, name, price, weight));
                        break;
                    case CHICKEN:
                        productList.add(new Chicken(id, name, price, weight));
                        break;
                }
            } catch (Exception e) {
                log.info("Weight can not be set to dairy product.");
            }

            try {
                int quantity = (int) rs.get(i).getValue(FieldValueEnum.QUANTITY.name());
                switch (className) {
                    case MILK:
                        productList.add(new Milk(id, name, price, quantity));
                        break;
                    case CHEESE:
                        productList.add(new Cheese(id, name, price, quantity));
                        break;
                }
            } catch (Exception e) {
                log.info("Quantity can not be set to meat product.");
            }
        }

        return productList;
    }

    public void subtractSingleProductQuantity(DSLContext context, Product product, int currentQuantity) {
        context.update(table(product.getType()))
                .set(field(FieldValueEnum.QUANTITY.name()), currentQuantity - 1)
                .where(field(FieldValueEnum.ID.name()).eq(product.getId()))
                .execute();
    }

    public void updateProductsQuantities(DSLContext context, Cart shopCart) {
        shopCart.getShopCart().forEach(product -> {
            if (product instanceof DairyProduct) {
                Result<Record> productRow = context.select()
                        .from(table(product.getType())).where(field(FieldValueEnum.ID.name())
                                .eq(product.getId())).fetch();

                int currentQuantity = (int) productRow.get(0).getValue(FieldValueEnum.QUANTITY.name());
                subtractSingleProductQuantity(context, product, currentQuantity);
            }
        });
    }
}