package shoppingcart.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import shoppingcart.Cart;

@Service
public class OrderDetailsService {
    public ResponseEntity<Object> showOrderDetails(Cart shopCart, BillCalculator billCalculator) {
        double totalBill = billCalculator.calculateShopCartBill(shopCart);

        return ResponseEntity.ok().body("Your cart: \n" + shopCart +
                "\nTotal bill: " + totalBill);
    }
}